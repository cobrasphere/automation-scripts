#!/bin/bash
# Creator: CobraSphere
#
SCRIPT_VER='0.1.0'

# Read Default Configuration and Parameters
source defaults.conf

# Read .provider file.
source cobrasphere.provider

# Script
sudo groupadd $TENANT_USERGROUP
echo -e "\nUser Group created as $TENANT_USERGROUP."

exit 0;
