#!/bin/bash
# Creator: CobraSphere
#
##

SCRIPT_VER='0.1.0'

# Check Parameters and Read Configuration
source check-params-read-config.sh

# Begin Setup

TENANT_USERNAME=$TENANT

sudo useradd $TENANT_USERNAME
echo -e "\nUser Created for $TENANT_USERNAME."

sudo usermod -a -G $TENANT_USERGROUP $TENANT_USERNAME
echo -e "\nUser Added to '$TENANT_USERGROUP' group."

groups $TENANT_USERNAME 

exit 0;

## Some old stuff
	# Create User is commented. Creating a user should be out of scope.
	# Create a new user!
    #sudo adduser $USERNAME
    #sudo adduser $USERNAME
    #echo "Please enter a password for the user: $USERNAME"
    #read -s PASS
    #sudo echo $PASS | sudo passwd --stdin $USERNAME
