#!/bin/bash
# Creator: CobraSphere

source defaults.conf

# Read .provider and .tenant files.
if [ "$#" -ne 2 ] ; then
  echo "Usage: $0 PROVIDER TENANT" >&2
  exit 1
fi

if ! [ -f "$1" ] ; then
  echo "Please ensure your .provider file exists." >&2
  exit 1
fi

if ! [ -f "$2" ] ; then
  echo "Please ensure your .tenant file exists." >&2
  exit 1
fi

source $1
source $2
