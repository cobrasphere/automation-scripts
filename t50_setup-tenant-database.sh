#!/bin/bash
# Creator: CobraSphere
#
##

SCRIPT_VER='0.2.3'

# Check Parameters and Read Configuration
source check-params-read-config.sh

# Begin Setup

DATABASE_NAME="$TENANT--$APP_NAME--$RND_KEY_16--$DEPLOY_TYPE"
DATABASE_USER="$TENANT--$APP_NAME--$RND_KEY_32"
DATABASE_PASS=$DB_PASS

echo -e "Creating PostgreSQL Database: '$DATABASE_NAME'."
createdb $DATABASE_NAME

echo -e "Creating PostgreSQL User    : '$DATABASE_USER'."
createuser --no-superuser --no-createdb --no-createrole --no-password $DATABASE_USER

SQL_UPASS="ALTER ROLE "\""$DATABASE_USER"\"" WITH PASSWORD '$DATABASE_PASS';"
SQL_CREATEDB="CREATE DATABASE "\""$DATABASE_NAME"\"" ENCODING 'UTF8';"
SQL_GRANT="GRANT ALL PRIVILEGES ON DATABASE "\""$DATABASE_NAME"\"" TO "\""$DATABASE_USER"\"";"


echo "---------------------------"
echo "Now running ... "
echo $SQL_UPASS
psql -c "$SQL_UPASS"
echo
echo "---------------------------"
echo "Now running ... "
echo $SQL_CREATEDB
psql -c "$SQL_CREATEDB"
echo
echo "---------------------------"
echo "Now running ... "
echo $SQL_GRANT
psql -c "$SQL_GRANT"
echo

echo "---------------------------"
echo "Database with name '$DATABASE_NAME' has been created."
echo "Permissions for '$DATABASE_USER' have been granted."
echo "---------------------------"
echo
echo "You should now setup your Django settings with the following:"
echo
echo "  'NAME': '$DATABASE_NAME',"
echo "  'ENGINE': 'django.db.backends.postgresql_psycopg2',"
echo "  'USER': '$DATABASE_USER',"
echo "  'PASSWORD': '$DATABASE_PASS',"
echo
echo "---------------------------"
echo

cd $TENANT_HOME_DIR_ROOT_PATH/$TENANT

DB_KEYS="$TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS/$TENANT--$DOMAIN--db.keys"

echo -e "$DATABASE_NAME:$DATABASE_USER:$DATABASE_PASS" | sudo tee -a $DB_KEYS
echo -e "The above has been saved to '$DB_KEYS'"

echo -e "Script name '$SCRIPT_NAME' (v$SCRIPT_VER) run on $DATE_HUMAN at $TIME." | sudo tee -a $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS/$LOG_FILE


cd $CURRENT_DIR

exit 0;


