#!/bin/bash
# Creator: CobraSphere
# Inspired by: Seb Dangerfield, http://sebdangerfield.me.uk
#
# Next steps:
# 1. Create Repo folder and initalise bare git repo
# 2. Change Git Repo HEAD to correct type (i.e. staging or production) git symbolic-ref HEAD refs/heads/staging
##

SCRIPT_VER='0.2.0'

# Check Parameters and Read Configuration
source check-params-read-config.sh

# Check if the username (TENANT) exists
if [ -z "$(getent passwd $TENANT)" ]; then
	echo "Username does not exist. Please create user first."
	exit 1
else
    echo "Found username: $TENANT"
    USERNAME=$TENANT
fi

# check the domain is valid!
PATTERN="^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$";
if [[ "$DOMAIN" =~ $PATTERN ]]; then
	DOMAIN=`echo $DOMAIN | tr '[A-Z]' '[a-z]'`
	echo "Creating hosting for:" $DOMAIN
else
	echo "invalid domain name"
	exit 1 
fi

# If we have got to this point without a failure (exit 1), let's setup the directory structure

# Create directory structure
cd $TENANT_HOME_DIR_ROOT_PATH/$USERNAME ## We don't want to use -p all the way from / (root).
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_CONF
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_LOGS
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_HTML
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_WSGI
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_STATIC
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_MEDIA
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_REPO
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS
sudo mkdir -p $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS/$TENANT_HOME_DIR_DOMAINS_TEMP
#sudo echo -e "Script name '$SCRIPT_NAME' (v$SCRIPT_VER) run on $DATE_HUMAN at $TIME." >> $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS/$LOG_FILE
echo -e "Script name '$SCRIPT_NAME' (v$SCRIPT_VER) run on $DATE_HUMAN at $TIME." | sudo tee -a $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_SYS/$LOG_FILE
cd $CURRENT_DIR


# Now we need to copy the virtual host template
#CONFIG=$NGINX_ALL_VHOSTS/$DOMAIN.conf
CONFIG=$TENANT_HOME_DIR_ROOT_PATH/$USERNAME/$TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_CONF/$DOMAIN.conf
ACCESSLOG=$TENANT_HOME_DIR_ROOT_PATH/$USERNAME/$TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_LOGS/$DOMAIN.access.log
ERRORLOG=$TENANT_HOME_DIR_ROOT_PATH/$USERNAME/$TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_LOGS/$DOMAIN.error.log

sudo cp $SCRIPT_DIR/templates/virtual_host.template $CONFIG
sudo $SED -i "s/DOMAIN/$DOMAIN/g" $CONFIG
sudo $SED -i "s#ROOT#$TENANT_HOME_DIR_ROOT_PATH\/$USERNAME\/$TENANT_HOME_DIR_DOMAINS\/$DOMAIN\/$TENANT_HOME_DIR_DOMAINS_HTML#g" $CONFIG
sudo $SED -i "s#ACCESSLOG#$ACCESSLOG#g" $CONFIG
sudo $SED -i "s#ERRORLOG#$ERRORLOG#g" $CONFIG

## Confirm if we actually need the following and then uncomment.
#sudo usermod -aG $USERNAME www-data
#sudo chmod g+rxs $TENANT_HOME_DIR_ROOT_PATH/$USERNAME
#sudo chmod 600 $CONFIG

# Test Nginx Configuration
sudo $NGINX -t
if [ $? -eq 0 ];then
	# Create symlinks
    sudo ln -s $CONFIG $NGINX_ALL_VHOSTS/$DOMAIN.conf
	sudo ln -s $CONFIG $NGINX_ENABLED_VHOSTS/$DOMAIN.conf
else
	echo "Could not create new vhost as there appears to be a problem with the newly created nginx config file: $CONFIG";
	exit 1;
fi

sudo service nginx reload

# put the template index.html file into the public_html dir!
#sudo mkdir $TENANT_HOME_DIR_ROOT_PATH/$TENANT_HOME_DIR/public_html

sudo cp $SCRIPT_DIR/templates/index.html.template $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_HTML/index.html
sudo $SED -i "s/SITE/$DOMAIN/g" $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_HTML/index.html
#sudo chown $USERNAME:$USERNAME $TENANT_HOME_DIR_DOMAINS/$DOMAIN/$TENANT_HOME_DIR_DOMAINS_HTML -R
sudo chown $USERNAME:$USERNAME $TENANT_HOME_DIR_DOMAINS -R

echo -e "\nSite Created for $DOMAIN"
echo "--------------------------"
echo "Host: "`hostname`
echo "URL:  $DOMAIN"
echo "User: $USERNAME"
echo "--------------------------"
exit 0;
